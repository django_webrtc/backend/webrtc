from django.urls import path
from web import views


app_name="web"

urlpatterns = [
    path("",views.index,name="index"),
    path("<str:room_name>/", views.room, name="room"),
]
