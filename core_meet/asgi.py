import os

from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
from channels.security.websocket import AllowedHostsOriginValidator
from django.core.asgi import get_asgi_application

import web.routing


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core_meet.settings')

django_asgi_app = get_asgi_application()

application = ProtocolTypeRouter({
    "http": django_asgi_app,
    "websocket": AllowedHostsOriginValidator(
            AuthMiddlewareStack(URLRouter(web.routing.websocket_urlpatterns))
        ),
    # Just HTTP for now. (We can add other protocols later.)
})